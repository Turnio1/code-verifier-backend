import server from './src/server'
import { LogError, LogSuccess } from './src/utils/logger'
// Environment variables
import dotenv from 'dotenv'


// Config .env file
dotenv.config()

const port = process.env.PORT || 8000

// Execute Server
server.listen(port, () =>{
  LogSuccess(`[SERVER ON]: Running in http://localhost:${port}/api`)
})

// Control Server Error
server.on('error', (error) => {
  LogError(`[SERVER ERROR]: ${error}` )
})