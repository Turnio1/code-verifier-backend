/**
 * Basic JSON controller response
 */
export type BasicResponse = {
  message: string
}
/**
 * Error response
 */
export type ErrorResponse = {
  error: string,
  message: string
}