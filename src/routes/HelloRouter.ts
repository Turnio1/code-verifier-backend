import express, { Request, Response } from "express"
import { HelloController } from "../controller/HelloController" 
import { LogInfo } from "../utils/logger"

// Router from express
let helloRouter = express.Router()

// http://localhost:8000/api/hello?name=Martin/

helloRouter.route('/')
  // GET:
  .get(async (req: Request, res: Response) =>{
    // Get query
    let name: any = req?.query?.name
    LogInfo(`Query param: ${name}`)
    // Controller instance to method execution
    const controller: HelloController = new HelloController()
    // Get response
    const response = await controller.getMessage(name)
    // Send response to client
    return  res.send(response)
}) 

// Export HelloRouter
export default helloRouter