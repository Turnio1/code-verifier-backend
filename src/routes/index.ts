/**
 * Root Router
 * Redirect to Routes
 */
import express, { Request, Response } from "express"
import helloRouter from "./HelloRouter"
import { LogInfo } from "../utils/logger"

// Server instance
let server = express()

// Router instance
let rootRouter = express.Router()

// Activate when request http://localhost:8000/api

rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo(`GET: http://localhost:8000/api`)
    // Send "Welcome"
    res.send('Welcome to API Restful: Express + TS + Swaggger + Mongoose')
})

// Redirect Routes & Controllers
server.use('/', rootRouter) // http://localhost:8000/api
server.use('/hello', helloRouter) // http://localhost:8000/api/hello --> HelloRouter

export default server